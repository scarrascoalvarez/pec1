/* Variables */
var gulp = require('gulp');
var del = require('del');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var sass = require('gulp-sass');
var sourcemaps  = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var ts = require('gulp-typescript');
var tsProject = ts.createProject('tsconfig.json');

/* Directorio de origen */
var rutaOrigen = {
  imagenes: 'src/img/',
  scripts: 'src/ts/',
  estilos: 'src/sass/',
  archivos: 'src/',
  vendor: 'src/vendor/',
  datos: 'src/data/'
};

/* Directorio de destino */
var rutaDestino = {
  imagenes: 'dist/img/',
  scripts: 'dist/js/',
  estilos: 'dist/css/',
  archivos: 'dist/',
  vendor: 'dist/vendor/',
  datos: 'dist/data/'
};

/* Optimizar imagenes */
gulp.task('optimizar_imagenes', function() {
  return gulp.src([rutaOrigen.imagenes+'**/*'])
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
    }))
    .pipe(gulp.dest(rutaDestino.imagenes))
    .pipe(browserSync.stream());
});

/* Conversión del codigo SASS a CSS */
gulp.task('conversion_sass', function() {
  return gulp.src([rutaOrigen.estilos+'**/*.scss'])
    .pipe(sourcemaps.init())
      .pipe(sass())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(rutaDestino.estilos))
    .pipe(browserSync.stream());
});

/* Limpiar directorio dist */
gulp.task('limpiar_dist', function(cb) {
  del([ rutaDestino.archivos+'*.html', rutaDestino.imagenes+'**/*', rutaDestino.scripts+'**/*', rutaDestino.estilos+'*.css', rutaDestino.vendor+'**/*', rutaDestino.datos+'**/*'], cb);
});

/* Conversión de typescript a javascript */
gulp.task('conversion_typescript', function() {
  var tsResult = gulp.src([rutaOrigen.scripts+'**/*.ts']) // or tsProject.src()
    .pipe(tsProject());
  return tsResult.js.pipe(gulp.dest(rutaOrigen.scripts));
});

/* Transferir HTML */
gulp.task('transferir_html', function() {
  return gulp.src([rutaOrigen.archivos+'*.html'])
    .pipe(gulp.dest(rutaDestino.archivos))
    .pipe(browserSync.stream());
});

/* Transferir vendor */
gulp.task('transferir_vendor', function() {
  return gulp.src([rutaOrigen.vendor+'**/*'])
    .pipe(gulp.dest(rutaDestino.vendor))
    .pipe(browserSync.stream())
});

/* Transferir data */
gulp.task('transferir_datos', function() {
  return gulp.src([rutaOrigen.datos+'**/*'])
    .pipe(gulp.dest(rutaDestino.datos))
    .pipe(browserSync.stream())
});

/* Procesamiento mediante JSHint para prevención de errores */
gulp.task('jshint', function() {
  return gulp.src([rutaOrigen.scripts+'**/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});

/* Concatenar y minificar tamaño JS */
gulp.task('uglify', ['jshint'], function() {
  return gulp.src([rutaOrigen.scripts+'**/*.js'])
    .pipe(sourcemaps.init())
      .pipe(concat('all.min.js'))
      .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest(rutaDestino.scripts))
    .pipe(browserSync.stream());
});

/* Build de la app */
gulp.task('build', ['transferir_datos', 'transferir_vendor', 'transferir_html', 'optimizar_imagenes', 'conversion_sass', 'conversion_typescript', 'uglify'], function() {
  browserSync.init({
    logLevel: "info",
        browser: ["safari", "google chrome", "Firefox"],
        proxy: "localhost:80",
        startPath: "todoApp/dist/"
  });
});

/* Tarea por defecto, limpiar el directorio de destino y hace el build de la app */
gulp.task('default', ['limpiar_dist', 'build'], function() {});
